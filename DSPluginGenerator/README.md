# DroidScript-JS-Plugin-Generator
A JavaScript Plugin generator for DroidScript

Just copy the JSPluginGen folder to the DroidScript projects folder (/sdcard/DroidScript/) and (re)start DroidScript

This template provides an instant plugin installer, deinstaller and exporter, as well as a quick plugin test area (you should rather create an extra DroidScript for that)

You shouldn't modify the JSPluginGen.js file as it may break the installation routines

Happy coding


Files
-----
```c
- JSPluginGen.js       template which can install, deinstall and export the Plugin
- Plugin.js           includes the plugin sources
- Documentation.html  the documentation template of all DroidScript plugins
```