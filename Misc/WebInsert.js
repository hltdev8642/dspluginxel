/*
Yes, that looks right https://developer.mozilla.org/en-US/docs/Web/API/Element/insertAdjacentHTML
*/
var html = (app.ReadFile('htmlA.html') + app.ReadFile('htmlB.html'))

function OnStart()
{
        lay = app.CreateLayout( "linear", "VCenter,FillXY" );        

        //Create a text label and add it to layout.
        web = app.CreateWebView( 1,0.5 );
        web.LoadHtml( html );
        lay.AddChild( web );
        
        btn = app.CreateButton( "test" );
        btn.SetOnTouch( btn_OnTouch );
              lst = app.CreateButton( "test" );
        lst.SetOnTouch( lst_OnTouch );
        lay.AddChild( btn );
        lay.AddChild(lst)
        txtN = app.CreateTextEdit()
        btnC = app.CreateButton("clear")
        btnC.SetOnTouch(function () { 
        
      
        app.WriteFile('htmlA.html',app.ReadFile( 'htmlAReset.html'))
        web.Reload()
        });
        lay.AddChild(btnC)
          lay.AddChild(txtN)
        app.AddLayout( lay );
}
function lst_OnTouch()
{
var name = txtN.GetText()
        
var cmd="var d1 = document.getElementById('one');"+


"d1.insertAdjacentHTML('afterend', '<x-menuitem><x-label>"
+ name 
+"</x-label></x-menuitem>');"
web.Execute( cmd );
app.WriteFile('htmlA.html', (app.ReadFile('htmlA.html')+ '<x-menuitem><x-label>' + name+'</x-label></x-menuitem>' + '\n'))
}
function btn_OnTouch()
{
var name = txtN.GetText()
        
var cmd="var d1 = document.getElementById('one');"+


"d1.insertAdjacentHTML('afterend', '<x-button><x-label>"
+ name 
+"</x-label></x-button>');"
web.Execute( cmd );
app.WriteFile('htmlA.html', (app.ReadFile('htmlA.html')+ '<x-button><x-label>' + name +'</x-label></x-button>' + '\n'))
}